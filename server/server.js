const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const db = require('./config/key').mlabUrl;
const users = require('./routes/api/users');

//initial server
const port = process.env.PORT || 5000;
const app = express();
const server = require('http').createServer(app);

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//connect DB
mongoose.connect(db,  { useNewUrlParser: true })
    .then(() => console.log("Connected DB!"))
    .catch(err => console.log(err));

app.use("/api", users)
app.use((req, res, next) => {
    res.status(404).json({error: "Api Not Found"})
});
//start server
server.listen(port, ()=> {
    console.log(`Server Running ${port}`);
})




