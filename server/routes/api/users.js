'use strict'
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const User = require('../../models/User');

router.get("/users",  (req, res) => {
    User.find({})
        .select("-password")
        .then(users => {
            if(users.length < 1) {
                res.status(404).json({error: "The User Table Is Empty"});
            } else {
                res.json(users);
            }
        })
        .catch(err => {
            res.status(500).json("Query Error");
        })
})

router.post("/users", (req, res) => {
    let newUser = new User(req.body);
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if(err){
                console.log(err);
                res.status(500).json({error: "Saving Password Failed"})
            }
            newUser.password = hash;
            newUser.save()
                .then(() => res.json("User Created!"))
                .catch(err => {
                    console.log(err);
                    res.status(500).json({error: "Error Occured While Saving"})
                });
        });
    });
    
})

router.route('/users/:user_id')
    .get((req, res) => {
        User.findById(req.params.user_id)
            .then(user => {
                if(!user) {
                    res.status(404).json({error: "The User Not Exist"});
                } else {
                    res.json(user);
                }
            })
            .catch(err => console.log(err))
    })
    .put((req, res) => {
        User.findById(req.params.user_id)
            .select("-password")
            .then(user => {
                if(!user) {
                    res.status(400).json({error: "The User Not Exist"});
                } else {
                    for(let props of Object.keys(req.body)) {
                        user[props] = req.body[props];
                    }
                    bcrypt.genSalt(10, (err, salt) => {
                        bcrypt.hash(user.password, salt, (err, hash) => {
                            if(err){
                                console.log(err);
                                res.status(500).json({error: "Saving Password Failed"})
                            }
                            user.password = hash;
                            user.save()
                                .then(() => res.json("User Updated!"))
                                .catch(err => {
                                    console.log(err);
                                    res.status(500).json({error: "Error Occured While Saving"})
                                });
                        });
                    });
                }
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({error: "Error Occured While Find The User"});
            })
    })
    .delete((req, res) => {
        User.deleteOne({_id: req.params.user_id})
            .then(() => res.json("User Deleted"))
            .catch(err => {
                console.log(err);
                res.status(500).json({error: "Error Occured While Delete User"});
            })
    })

module.exports = router;