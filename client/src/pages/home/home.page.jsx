import React from 'react';
import UserTable from '../../components/user-table/user-table.component';
import Pager from '../../components/pager/pager.component';

import './home.styles.css';

const Home = () => (
    <div className="home">
        <h2>Users</h2>
        <UserTable />
        <Pager />
    </div>
)

export default Home;