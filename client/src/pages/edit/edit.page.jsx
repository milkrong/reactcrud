import React from 'react';
import { connect } from 'react-redux';
import { getUser, resetUser } from '../../redux/user/user.actions';
import InputForm from '../../components/input-form/input-form.component';
import { Redirect } from 'react-router-dom';

class Edit extends React.Component {

    componentDidMount() {
        this.props.getUser(this.props.match.params.userId);
    }

    render() {
        const { userState } = this.props;
        if (userState.userSubmitted) {
            this.props.resetUser();
            return (
                <Redirect to="/" />
            )
        }
        return (
            <div className="edit">
                <h2>
                    Edit User 
                </h2>
                <InputForm userState={userState}/>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    userState: state.user
})

const mapDispatchToProps = dispatch => ({
    resetUser: () => dispatch(resetUser()),
    getUser: id => dispatch(getUser(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(Edit);