import React from 'react';
import InputForm from '../../components/input-form/create-form.component';
import { connect } from 'react-redux';
import { resetUser } from '../../redux/user/user.actions'
import { Redirect } from 'react-router-dom';

class Create extends React.Component {
    render() {
        const { userState } = this.props;
        if (userState.userSubmitted) {
            this.props.resetUser();
            return (
                <Redirect to="/" />
            )
        }
        return (
            <div className="create">
                <h2>
                    Create User 
                </h2>
                <InputForm />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    userState: state.user
})

const mapDispatchToProps = dispatch => ({
    resetUser: () => dispatch(resetUser())
})

export default connect(mapStateToProps, mapDispatchToProps)(Create);