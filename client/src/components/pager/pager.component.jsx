import React from 'react';
import { connect } from 'react-redux';
import { Pagination } from 'element-react';
import { changePage } from '../../redux/pager/pager.actions';
import './pager.styles.css';

const Pager = ({userState, pagerState, changePage}) => {
    const total = userState.filtered.length;
    const { pageSize } = pagerState;

    return (
        <div className="pager">
            <Pagination layout="prev, pager, next" 
                total={total} 
                pageSize={pageSize} 
                onCurrentChange={e => changePage(e)}
                currentPage = {pagerState.currentPage}
            />
        </div>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        changePage: currentPage => dispatch(changePage(currentPage))
    }
}

const mapStateToProps = state => {
    return {
        pagerState : state.pager,
        userState: state.users
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Pager);