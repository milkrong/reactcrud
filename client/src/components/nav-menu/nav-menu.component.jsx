import React from 'react';
import { Menu } from 'element-react';
import { withRouter } from 'react-router-dom';

const NavMenu = (props) => (
    <div className="nav-menu">
        <Menu theme="dark" defaultActive="1" mode="horizontal" onSelect={index => props.history.push("/")}>
            <Menu.Item index="1">Home</Menu.Item>
        </Menu>
    </div>
)

export default withRouter(NavMenu);