import React from 'react';
import { Form, Input, Button, InputNumber, Radio } from 'element-react';
import { createUser } from '../../redux/user/user.actions';
import { connect } from 'react-redux';

import './input-form.style.css';

class InputForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            form: {
                firstName: '',
                lastName: '',
                sex: '',
                age: 0,
                password: '',
                repassword: ''
            },
            rules: {
                firstName: [
                    { required: true, message: 'Please enter your first name', trigger: 'blur' }
                ],
                lastName: [
                    { required: true, message: 'Please enter your last name', trigger: 'blur' }
                ],
                sex: [
                    { required: true, message: 'Please chose your first name', trigger: 'change' }
                ],
                age: [
                    { type:"number", required: true, message: 'Please set your age', trigger: 'change' }
                ],
                password: [
                    { required: true, message: 'Please enter your password', trigger: 'blur' },
                    { validator: (rule, value, callback) => {
                    if (value === '') {
                        callback(new Error('Please enter your password'));
                    } else {
                        if (this.state.form.repassword !== '') {
                        this.refs.form.validateField('repassword');
                        }
                        callback();
                    }
                    } }
                ],
                repassword: [
                    { required: true, message: 'Please confirm your password', trigger: 'blur' },
                    { validator: (rule, value, callback) => {
                        if (value !== this.state.form.password) {
                            callback(new Error('The password is not the same'));
                        } else {
                            callback();
                        }
                    } }
                ]
            }
        }
    }

    handleChange = (key, value) => {
        this.setState({
            form: Object.assign({}, this.state.form, { [key]: value })
        })
    }

    handleSubmit = (user, e) => {
        e.preventDefault();
        this.refs.form.validate((valid) => {
            if (valid) {
                this.props.createUser(user);
            } else {
              console.log('error submit!!');
              return false;
            }
        });
    }

    render () {
        return (
            <div className="input-form">
                <Form ref="form" model={this.state.form} labelPosition={"top"} rules={this.state.rules}>
                    <Form.Item label="First Name" prop="firstName">
                        <Input value={this.state.form.firstName} onChange={value => this.handleChange("firstName", value)}></Input>
                    </Form.Item>
                    <Form.Item label="Last Name" prop="lastName">
                        <Input value={this.state.form.lastName} onChange={value => this.handleChange("lastName", value)}></Input>
                    </Form.Item>
                    <Form.Item label="Sex" prop="sex">
                        <Radio.Group onChange={value => this.handleChange("sex", value)}>
                            <Radio value="female" checked={this.state.form.sex === "female"}>Female</Radio>
                            <Radio value="male" checked={this.state.form.sex === "male"}>Male</Radio>
                        </Radio.Group>
                    </Form.Item>
                    <Form.Item label="Age" prop="age">
                        <InputNumber type="number" defaultValue={this.state.form.age} value={this.state.form.age} onChange={value => this.handleChange("age", value)}></InputNumber>
                    </Form.Item>
                    <Form.Item label="Password" prop="password">
                        <Input type="password" value={this.state.form.password} onChange={value => this.handleChange("password", value)}></Input>
                    </Form.Item>
                    <Form.Item label="Retype Password" prop="repassword">
                        <Input  type="password" value={this.state.form.repassword} onChange={value => this.handleChange("repassword", value)}></Input>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" nativeType="submit" onClick={e => this.handleSubmit(this.state.form, e)} >Submit</Button>
                        <Button type="warning" onClick={()=> this.props.history.goBack()}>Cancel</Button>
                    </Form.Item>
                </Form>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    createUser: (user) => dispatch(createUser(user))
})


export default connect(null, mapDispatchToProps)(InputForm);