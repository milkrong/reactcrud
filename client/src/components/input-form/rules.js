export const createRules = {
    firstName: [
        { required: true, message: 'Please enter your first name', trigger: 'blur' }
    ],
    lastName: [
        { required: true, message: 'Please enter your last name', trigger: 'blur' }
    ],
    sex: [
        { required: true, message: 'Please chose your first name', trigger: 'change' }
    ],
    age: [
        { required: true, message: 'Please set your age', trigger: 'change' }
    ],
    password: [
        { required: true, message: 'Please enter your password', trigger: 'blur' },
        { validator: (rule, value, callback) => {
          if (value === '') {
            callback(new Error('Please enter your password'));
          } else {
            if (this.state.form.repassword !== '') {
              this.refs.form.validateField('repassword');
            }
            callback();
          }
        } }
    ],
    repassword: [
        { required: true, message: 'Please confirm your password', trigger: 'blur' },
        { validator: (rule, value, callback) => {
            if (value !== this.state.form.password) {
                callback(new Error('The password is not the same'));
            } else {
                callback();
            }
        } }
    ]
}