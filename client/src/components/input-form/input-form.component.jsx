import React from 'react';
import { Form, Input, Button, InputNumber, Radio } from 'element-react';
import { changeUser, updateUser } from '../../redux/user/user.actions';
import { connect } from 'react-redux';

import './input-form.style.css';

class InputForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            form: {
                password: '',
                repassword: ''
            },
            rules: {
                repassword: [
                    { trigger: 'blur' },
                    { validator: (rule, value, callback) => {
                        console.log(value)
                        if (value !== this.state.form.password) {
                            callback(new Error('password is different!'));
                        } else {
                            callback();
                        }
                    
                    } }
                ]
            }
        }
    }

    handlePassword = (key, value) => {
        this.setState({
            form: Object.assign({}, this.state.form, { [key]: value })
        })
    }

    handleSubmit = (user, e) => {
        e.preventDefault();
        this.refs.form.validate((valid) => {
            if (valid) {
                if (this.state.form.password.length > 0) {
                    user = {
                        ...user,
                        password: this.state.form.password
                    };
                }
                this.props.updateUser(user); 
            } else {
              console.log('error submit!!');
              return false;
            }
        });
    }

    render () {
        const { userState, handleChange } = this.props;
        const user = userState.user;
        return (
            <div className="input-form">
                <Form model={user} labelPosition={"top"}>
                    <Form.Item label="First Name" prop="firstName">
                        <Input value={user.firstName} onChange={value => handleChange("firstName", value)}></Input>
                    </Form.Item>
                    <Form.Item label="Last Name" prop="lastName">
                        <Input value={user.lastName} onChange={value => handleChange("lastName", value)}></Input>
                    </Form.Item>
                    <Form.Item label="Sex" prop="sex">
                        <Radio.Group onChange={value => handleChange("sex", value)}>
                            <Radio value="female" checked={user.sex === "female"}>Female</Radio>
                            <Radio value="male" checked={user.sex === "male"}>Male</Radio>
                        </Radio.Group>
                    </Form.Item>
                    <Form.Item label="Age" prop="age">
                        <InputNumber defaultValue={user.age} value={user.age} onChange={value => handleChange("age", value)}></InputNumber>
                    </Form.Item>
                </Form>
                <Form ref="form" model={this.state.form} rules={this.state.rules} labelPosition={"top"}>
                    <Form.Item label="Password" prop="password">
                        <Input type="password" value={this.state.form.password} onChange={value => this.handlePassword("password", value)}></Input>
                    </Form.Item>
                    <Form.Item label="Retype Password" prop="repassword">
                        <Input  type="password" value={this.state.form.repassword} onChange={value => this.handlePassword("repassword", value)}></Input>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" nativeType="submit" onClick={e => this.handleSubmit(user, e)} >Submit</Button>
                        <Button type="warning" onClick={() => this.props.history.goBack()}>Cancel</Button>
                    </Form.Item>
                </Form>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    handleChange: (key, value) => dispatch(changeUser(key, value)),
    updateUser: (user) => dispatch(updateUser(user))
})

export default connect(null, mapDispatchToProps)(InputForm);