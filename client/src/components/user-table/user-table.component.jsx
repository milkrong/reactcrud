import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getUsers } from '../../redux/users/users.actions';
import { sortUser } from '../../redux/users/users.actions'; 
import { Table, Loading, Button } from 'element-react';
import ActionButton from '../action-button/action-button.component';
import SearchBox from '../search-box/search-box.component';
import './user-table.style.css';

class UserTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loadingText: 'Fetching All Users',
            columns: [
                {
                    type: 'index'
                },
                {
                    label: "Edit",
                    prop: "edit",
                    render: function(data){
                      return (
                        <ActionButton id={data._id} action="edit"/>
                      )
                    }
                },
                {
                    label: "Delete",
                    prop: "delete",
                    render: function(data){
                      return (
                        <ActionButton id={data._id} action="delete"/>
                      )
                    }
                },
                {
                    label: "First Name",
                    prop: "firstName",
                    sortable: true,
                    width: 150
                },
                {
                    label: "Last Name",
                    prop: "lastName",
                    sortable: true,
                    width: 150
                },
                {
                    label: "Sex",
                    prop: "sex"
                },
                {
                    label: "Age",
                    prop: "age",
                    sortable: true
                }
            ]
        }
    }

    componentDidMount() {
        const { getUsers } = this.props;
        getUsers();
    }

    sortColumn = obj => {
        const { prop, order } = obj;
        const { sortUser } = this.props;
        sortUser(prop, order === "descending");
    }

    render () {
        const { userState } = this.props;
        const { pagerState } = this.props;
        let start = (pagerState.currentPage - 1) * pagerState.pageSize;
        let end = pagerState.currentPage * pagerState.pageSize;
        const showData = userState.filtered.slice(start, end)
        return (
            <div className="user-table">
                <div className="table-title">
                    <SearchBox />
                    <div className="add-button">
                        <Button 
                            plain={true} 
                            type="info" 
                            icon="plus"
                            onClick={()=> this.props.history.push("/create")}
                        >
                            Add
                        </Button>
                    </div>
                </div>
                <Loading loading={userState.isLoading} text={this.state.loadingText}>
                    <Table data = {showData} columns={this.state.columns} onSortChange={obj => this.sortColumn(obj)}/>
                </Loading>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        userState: state.users,
        pagerState: state.pager
    };
}

const mapDispatchToProps = dispatch => {
    return {
        sortUser : (columnKey, isDes) => dispatch(sortUser(columnKey, isDes)),
        getUsers : () => dispatch(getUsers())
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UserTable));