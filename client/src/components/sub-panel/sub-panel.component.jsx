import React from 'react';
import { Alert } from 'element-react';
import { connect } from 'react-redux';

const SubPanel = props => {
    const { error } = props
    return (
        <div className="sub-panel">
            {error.message && <Alert title={error.errorType} type="error" description={error.message} />}
        </div>
    )
}


const mapStateToProps = state => ({
    error: state.error
})

export default connect(mapStateToProps)(SubPanel);