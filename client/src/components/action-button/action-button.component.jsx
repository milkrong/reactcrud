import React from 'react';
import { connect } from 'react-redux';
import { deleteUser } from '../../redux/users/users.actions';
import { Button } from 'element-react';
import { withRouter } from 'react-router-dom';

class ActionButton extends React.Component {
    handleClick = (action, id) => {
        if (action === "delete") {
            this.props.deleteUser(id)
        } else {
            this.props.history.push(`/edit/${id}`);
        }
    }
    render () {
        const { id, action } = this.props;
        return (
            <div>
                <span>
                    <Button type={action === "delete" ? "danger": "info"} 
                        size="small" 
                        onClick={() => this.handleClick(action, id)}>
                        {action.toUpperCase()}
                    </Button>
                </span>
            </div>
        )
    }
}

const mapDeleteToProps = dispatch => ({
    deleteUser: userId => dispatch(deleteUser(userId))
    }
)
export default withRouter(connect(null, mapDeleteToProps)(ActionButton));