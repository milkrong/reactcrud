import React from 'react';
import { searchUser } from '../../redux/users/users.actions';
import { Input, Button } from 'element-react';

import './search-box.style.css';
import { connect } from 'react-redux';

class SearchBox extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            keyword: ''
        }
    }

    handleChange = e => {
        this.setState({keyword: e}, () => {
            this.props.searchUser(this.state.keyword);
        });
    }

    render () {
        const {searchUser} = this.props;
        return (
            <div className="search-box">
                <Input 
                    placeholder="Search users" 
                    onChange = {this.handleChange}
                    append={<Button 
                        type="primary" 
                        icon="search" 
                        disabled={this.state.keyword > 0}
                        onClick={()=> searchUser(this.state.keyword)}
                        >
                            Search
                        </Button>} 
                />
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    searchUser: keyword => dispatch(searchUser(keyword))
})

export default connect(null, mapDispatchToProps)(SearchBox);