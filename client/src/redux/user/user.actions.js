import UserActionTypes from './user.types';
import { setNetworkError } from '../error/error.actions';
import axios from 'axios';

// Edit User
const getUserStart = () => {
    return {
        type: UserActionTypes.GET_ONE_START
    }
}

const getUserSuccess = (user) => {
    return {
        type: UserActionTypes.GET_ONE_SUCCESS,
        user
    }
}

export const getUser = (userId) => {
    return (dispatch) => {
        dispatch(getUserStart());
        axios.get(`http://localhost:5000/api/users/${userId}`)
            .then(res => {
                dispatch(getUserSuccess(res.data));
            })
            .catch(err => {
                console.log(err)
                dispatch(setNetworkError(err.message));
            });
    }
}

//change value
export const changeUser = (key, value) => {
    return {
        type: UserActionTypes.CHANGE_VALUE,
        payload: {
            [key] : value
        }
    }
}

// submit user action combination
const submitUserStart = () => ({
    type: UserActionTypes.SUBMIT_USER_START
})

const submitUserSucess = () => ({
    type: UserActionTypes.SUBMIT_USER_SUCCESS
})

export const createUser = user =>  {
    return (dispatch) => {
        dispatch(submitUserStart());
        axios.post(`http://localhost:5000/api/users`, user, {
                headers : {
                    "Content-Type": "application/json"
                }
            }).then(res => {
                if(res.status === 200) {
                    dispatch(submitUserSucess());
                }
            }).catch(err => {
                console.log(err)
                dispatch(setNetworkError(err.message));
        })
    }
}

export const updateUser = newUser =>  {
    return (dispatch) => {
        dispatch(submitUserStart());
        axios.put(`http://localhost:5000/api/users/${newUser._id}`, newUser, {
                headers : {
                    "Content-Type": "application/json"
                }
            }).then(res => {
                if(res.status === 200) {
                    dispatch(submitUserSucess());
                }
            }).catch(err => {
                console.log(err)
                dispatch(setNetworkError(err.message));
        })
    }
}

export const resetUser = () => ({
    type: UserActionTypes.RESET_STATE
})