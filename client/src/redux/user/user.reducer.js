import UserActionTypes from './user.types';

const INITIAL_STATE = {
    isLoading: false,
    user:{},
    userSubmitted: false
}

const userReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case UserActionTypes.GET_ONE_START:
            return {
                ...state,
                isLoading: true
            };
        case UserActionTypes.GET_ONE_SUCCESS:
            return {
                ...state,
                isLoading: false,
                user: action.user
            };
        case UserActionTypes.CHANGE_VALUE:
            return {
                ...state,
                user: {
                    ...state.user,
                    ...action.payload
                }
            }
        case UserActionTypes.SUBMIT_USER_START:
            return {
                ...state,
                userSubmitted: false
            }
        case UserActionTypes.SUBMIT_USER_SUCCESS:
            return {
                ...state,
                userSubmitted: true
            }
        case UserActionTypes.RESET_STATE:
            return {
                ...state,
                ...INITIAL_STATE
            }
        default:
            return state;
    }
}

export default userReducer;
