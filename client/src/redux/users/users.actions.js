import UsersActionTypes from './users.types';
import { setNetworkError } from '../error/error.actions';
import axios from 'axios';
import { changePage } from '../pager/pager.actions';

const getUsersStart = () => {
    return {
        type: UsersActionTypes.GET_USER_START
    }
}

const getUsersSuccess = (users) => {
    return {
        type: UsersActionTypes.GET_USER_SUCCESS,
        users
    }
}

export const searchUser = keyword => {
    return {
        type: UsersActionTypes.SEARCH_USER,
        keyword 
    }
}

export const updateFiltered = () => {
    return {
        type: UsersActionTypes.UPDATE_FILTERED,
    }
}

export const getUsers = () => {
    return (dispatch, getState) => {
        dispatch(getUsersStart());
        axios.get('http://localhost:5000/api/users/')
            .then(res => {
                dispatch(updateFiltered());
                dispatch(changePage(1));
                dispatch(getUsersSuccess(res.data));
            })
            .catch(err => {
                dispatch(setNetworkError(err.message));
            });
    }
}

// Delete User

const deleteUserStart = () => {
    return {
        type: UsersActionTypes.DEL_USER_START
    }
}

const deleteUserSuccess = (userId) => {
    return {
        type: UsersActionTypes.DEL_USER_SUCCESS,
        userId
    }
}

export const deleteUser = (userId) => {
    return (dispatch) => {
        dispatch(deleteUserStart());
        axios.delete(`http://localhost:5000/api/users/${userId}`)
            .then(res => {
                console.log(res);
                if (res.status === 200) {
                    dispatch(getUsers());
                }
                else {
                    dispatch(setNetworkError(res.data.error))
                }
            })
            .catch(err => {
                dispatch(setNetworkError(err.message)); 
            });
    }
}

// Sort 
export const sortUser = (columnKey, isDes) => {
    return {
        type: UsersActionTypes.SORT_USERS,
        payload: {
            key: columnKey,
            isDes: isDes
        }
    }
}