export const searchUser = (users, keyword) => {
    if(keyword.length < 1) {
        return users;
    }
    const result = users.filter(
        user => user.firstName.indexOf(keyword) !== -1 || user.lastName.indexOf(keyword) !== -1
    )
    console.log(result);
    return result;
}

export const deleteUser = (users, userId) => {
    const result = users.filter(
        user => user._id !== userId
    )
    console.log(result);
    return result;
}

export const sortUser = (users, key, isDes) => {
    let res = []
    if(isDes) {
        res = users.sort((a, b) => {
            if (a[key] > b[key]) {
                return -1;
            } 
            if (a[key < b[key]]) {
                return 1
            }
            return 0;
        });
    } else {
        res = users.sort((a, b) => {
            if (a[key] > b[key]) {
                return 1;
            } 
            if (a[key] < b[key]) {
                return -1
            }
            return 0;
        });
    }
    return res;
}