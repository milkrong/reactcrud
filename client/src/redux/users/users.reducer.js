import UsersActionTypes from './users.types';
import {searchUser, deleteUser, sortUser} from './users.utils';

const INITIAL_STATE = {
    isLoading: false,
    users:[],
    filtered: []
}

const usersReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case UsersActionTypes.GET_USER_START:
            return {
                ...state,
                isLoading: true
            };
        case UsersActionTypes.GET_USER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                users: action.users,
                filtered: action.users
            };
        case UsersActionTypes.DEL_USER_START:
            return {
                ...state,
                isLoading: true
            };
        case UsersActionTypes.DEL_USER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                users: deleteUser(state.users, action.userId)
            };
        case UsersActionTypes.SEARCH_USER:
            return {
                ...state,
                filtered: searchUser(state.users, action.keyword)
            };
        case UsersActionTypes.UPDATE_FILTERED:
            return {
                ...state,
                filtered: state.users
            };
        case UsersActionTypes.SORT_USERS:
            return {
                ...state,
                filtered: sortUser(state.filtered, action.payload.key, action.payload.isDes)
            }
        default:
            return state;
    }
}

export default usersReducer;