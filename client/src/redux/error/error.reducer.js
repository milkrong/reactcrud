import ErrorType from './error.type';

const INITIAL_STATE = {
    errorType:'',
    message: ''
}

const errorReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ErrorType.NETWORK_ERROR:
            return {
                ...state,
                errorType: 'Network Error',
                message: action.message
            }
    
        default:
            return state;
    }
}

export default errorReducer;