import ErrorType from './error.type';

export const setNetworkError = message => ({
    type: ErrorType.NETWORK_ERROR,
    message: message
})