import { combineReducers } from 'redux';
import usersReducer from './users/users.reducer';
import userReducer from './user/user.reducer';
import pagerReducer from './pager/pager.reducer';
import errorReducer from './error/error.reducer';

export default combineReducers({
    users: usersReducer,
    user: userReducer,
    pager: pagerReducer,
    error: errorReducer
});