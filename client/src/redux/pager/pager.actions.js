import PagerActionTypes from './pager.types';

export const changePage = currentPage => {
    return {
        type: PagerActionTypes.CHANGE_PAGE,
        currentPage
    }
}