import PagerActionTypes from './pager.types';

const INITIAL_STATE = {
    currentPage: 1,
    pageSize: 5
}

const pagerReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case PagerActionTypes.CHANGE_PAGE:
            return {
                ...state,
                currentPage: action.currentPage
            };
        default:
            return state;
    }
}

export default pagerReducer;