import React from 'react';
import { Switch, Route } from 'react-router-dom'
import NavMenu from './components/nav-menu/nav-menu.component';
import SubPanel from './components/sub-panel/sub-panel.component';
import Home from './pages/home/home.page';
import Edit from './pages/edit/edit.page';
import Create from './pages/create/create.page';
import './App.css';


const App = () => {
  return (
    <div className="app">
      <NavMenu />
      <div className="main">
        <SubPanel />
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/edit/:userId" component={Edit}/>
          <Route exact path="/create" component={Create}/>
        </Switch>
      </div>
    </div>
  );
}


export default App;
