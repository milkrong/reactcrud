import React from 'react';
import { Switch, Route } from 'react-router-dom'
import './App.css';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" />
        <Route exact path="/edit/:userId" />
        <Route exact path="/create" />
      </Switch>
    </div>
  );
}

export default App;
